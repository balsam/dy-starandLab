package com.gdut.llc.controllers.login;


import com.gdut.llc.pojo.User;
import com.gdut.llc.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**登录controller
 * Created by duliang on 2016/5/31.
 */

@Controller
@RequestMapping("/index")
public class LoginController {

    private static final Logger LOG = Logger.getLogger(LoginController.class);

    @Resource
    private UserServiceImpl userService;
    /**
     * 返回index.jsp
     * @return
     */
    @RequestMapping("/")
    public String index(){
        System.out.println("test index = ");
        return "index";
    }

    /**
     * 返回登录结果
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public Map<String, Object> signIn(String username, String password, HttpSession session) {
        Map<String,Object> rsp=new HashMap<String, Object>();
        User user = userService.getUserById(1);
        System.out.println("user = " + user.getName() + user.getId() + user.getPassword());
        if (username == null || password == null) {
            rsp.put("success", false);
            rsp.put("message", "密码或者账号为null");
            rsp.put("username", user.getName());

            return rsp;
        }

        rsp.put("success",true);
        System.out.println("rsp = " + rsp);
        return rsp;

    }


}
