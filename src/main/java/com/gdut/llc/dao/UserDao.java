package com.gdut.llc.dao;

import com.gdut.llc.pojo.User;
import com.gdut.llc.pojo.UserExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);


    User selectByPrimaryKey(Integer id);

    User selectByUserName(String name);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}