package com.gdut.llc.service.itf;

import com.gdut.llc.pojo.Group;

/**
 * Group接口
 * Created by duliang on 2016/6/1.
 */
public interface IGroupService {

    Group getGroupById(int groupId);

    Group getGroupByName(String name);

    int insert(Group record);
}
