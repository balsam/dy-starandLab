package com.gdut.llc.service.itf;


import com.gdut.llc.pojo.User;

/**
 * user接口
 * Created by duliang on 2016/6/1.
 */
public interface IUserService {

    public User getUserById(int userId);

    public User getUserByName(String name);

    public int insert(User record);

    void userAdd(User user);

    User getUser(String key);
}
