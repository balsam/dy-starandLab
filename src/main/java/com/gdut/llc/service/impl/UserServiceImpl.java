package com.gdut.llc.service.impl;

import com.gdut.llc.dao.UserDao;
import com.gdut.llc.pojo.User;
import com.gdut.llc.service.itf.IUserService;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


/**
 * Created by duliang on 2016/6/1.
 */

@Service("userService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SqlSessionFactoryBean sqlSessionFactory;

    @Autowired
    private RedisTemplate redisTemplate;


    public User getUserById(int userId) {
        System.out.println("userId = " + userId);
        return this.userDao.selectByPrimaryKey(userId);

    }

    public User getUserByName(String name) {
        // TODO: 2016/6/4 缓存操作
        User user = new User();
        System.out.println("============= = ");
        user.setName("test");
        return this.userDao.selectByUserName(name);

    }

    public int insert(User record) {
        return 0;
    }

    public void userAdd(User user) {

    }

    public User getUser(String key) {
        return null;
    }


/*
   public void userAdd(User user) {
       // TODO Auto-generated method stub
        *//*
10          * boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
11          * public Boolean doInRedis(RedisConnection redisConnection) throws
12          * DataAccessException { RedisSerializer<String> redisSerializer =
13          * redisTemplate .getStringSerializer(); byte[] key =
14          * redisSerializer.serialize(user.getId()); byte[] value =
15          * redisSerializer.serialize(user.getName()); return
16          * redisConnection.setNX(key, value); } }); return result;
17          *//*







               ValueOperations<String, User> valueops = redisTemplate
                        .opsForValue();
                valueops.set(user.getName(), user);
    }

    public User getUser(String key) {
        ValueOperations<String, User> valueops = redisTemplate
                         .opsForValue();
                 User user = valueops.get(key);
                 return user;
    }*/
}
