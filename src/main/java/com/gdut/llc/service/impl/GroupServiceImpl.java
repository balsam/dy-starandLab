package com.gdut.llc.service.impl;

import com.gdut.llc.dao.GroupDao;
import com.gdut.llc.pojo.Group;
import com.gdut.llc.service.itf.IGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * Created by duliang on 2016/6/1.
 */

@Service("groupService")
public class GroupServiceImpl implements IGroupService {

    @Resource
    private GroupDao groupDao;


    /**
     * @param groupId
     * @return
     */
    public Group getGroupById(int groupId) {

        return this.groupDao.selectByPrimaryKey(groupId);
    }

    /**
     * @param name
     * @return
     */
    public Group getGroupByName(String name) {
        return null;
    }

    /**
     * @param record
     * @return
     */
    public int insert(Group record) {
        return 0;
    }


}
