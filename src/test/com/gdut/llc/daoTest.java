package com.gdut.llc;

import com.alibaba.fastjson.JSON;
import com.gdut.llc.pojo.Group;
import com.gdut.llc.pojo.User;
import com.gdut.llc.service.impl.GroupServiceImpl;
import com.gdut.llc.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * dao 测试类
 * Created by duliang on 2016/6/1.
 */

@RunWith(SpringJUnit4ClassRunner.class)     //表示继承了SpringJUnit4ClassRunner类
@ContextConfiguration(locations = {"classpath:config/spring-mybatis.xml", "classpath:config/spring-redis.xml"})
//@ContextConfiguration(locations = {"classpath:config/spring-redis.xml"})
public class daoTest {

    private static Logger logger = Logger.getLogger(daoTest.class);

    @Resource
    private UserServiceImpl userService = null;

    @Resource
    private GroupServiceImpl groupService = null;


    @Test
    public void testUser() {

        User user = userService.getUserById(1);
        System.out.println("user = " + user.getName());
        User user1 = userService.getUserByName("王凯彬");
        System.out.println("user1 = " + user1.getEmail());
        logger.info(JSON.toJSONString(user));
    }


    @Test
    public void testGroup() {
        System.out.println("00000000 = " + 00000000);
        Group group = groupService.getGroupById(1);
        System.out.println("group = " + group);
        System.out.println("user = " + group.getId());
        logger.info(JSON.toJSONString(group));
    }
}
