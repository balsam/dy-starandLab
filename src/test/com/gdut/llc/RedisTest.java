package com.gdut.llc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by duliang on 2016/6/3.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:config/spring-redis.xml"})
public class RedisTest {

    @Autowired
    RedisTemplate redisTemplate;

    @Test
    public void test1() {
        System.out.println("================");
        redisTemplate.opsForValue().set("redis", "test");
        System.out.println("存储");
        redisTemplate.opsForValue().get("redis");
        System.out.println("===============");
        System.out.println("redisTemplate.opsForValue().get(\"redis\") = " + redisTemplate.opsForValue().get("redis"));

    }

}
