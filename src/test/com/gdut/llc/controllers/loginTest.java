package com.gdut.llc.controllers;

import com.gdut.llc.controllers.login.LoginController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by duliang on 2016/6/1.
 */
@RunWith(SpringJUnit4ClassRunner.class)     //表示继承了SpringJUnit4ClassRunner类
@ContextConfiguration(locations = {"classpath:config/spring-mybatis.xml"})
public class loginTest {

    @Test
    public void SignInTest() {
        LoginController login = new LoginController();

        login.signIn(null, null, null);
    }
}
