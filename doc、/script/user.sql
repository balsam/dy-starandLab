DROP TABLE IF EXISTS `user`;
CREATE TABLE `dy_tqs`.`user`   (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  `group_id` VARCHAR(45) NULL,
  `user_level` VARCHAR(45) NULL,
  `nickname` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
COMMENT = '用户表';


CREATE TABLE `dy_tqs`.`group`  if not exists(
  `group_id` INT NOT NULL,
  `group_name` VARCHAR(45) NULL,
  `group_info` VARCHAR(45) NULL,
  PRIMARY KEY (`group_id`))
COMMENT = '用户组表';

DROP TABLE IF EXISTS `stdvalue`;
CREATE TABLE `stdvalue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TaskID` int(11) NOT NULL,
  `ProcCode` int(11) NOT NULL,
  `FirstChkItemID` int(11) NOT NULL,
  `Result` decimal(7,3) NOT NULL,
  `ErrorValue` decimal(7,3) NOT NULL DEFAULT '0.000',
  `CheckStdType` tinyint(4) NOT NULL,
  `RecordTime` datetime NOT NULL,
  `RecordUserId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `CHECKITEM` (`TaskID`,`ProcCode`,`FirstChkItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

